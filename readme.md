# Topic : CLOSURES 

To understand closures, we will first look at Scopes
## Scope :
It defines where an item( like variable, constant) is accessible. Let's understand with examples :

```js
const a = 10;
console.log( a );
```
In the above example, The scope of variable `a` is accessible throughout the program.

Now let's look at the types of scopes :
1) Global scope
2) local scope


Global scope : 
These variables are accessible throughout the program.
In the previous example we discussed variable `a` is accessible throughout the program. So variable `a` is in global scope.

local scope :
These variables are accessible throughout the function or block. Let's look at the example 
```js
function calledFunction() {
    let b = 10;
    console.log(b); // prints 10
    // here variable b is accessible 
}
calledFunction();
console.log(b); // throws an error
// here variable b is not accessible 
```
Let's understand, why does the variable `b` is not accessible outside the function ?

For the above question, the answer would be whenever the `calledFunction()` function is called. That function is pushed into the execution stack and after the `calledFunction()` function finishes its execution it will be pop from the execution stack. As soon as, the function is popped, the variables which are local to function are deleted from the memory.
So, variable `b` is not accessible outside the function because it is not in the memory.

Let's take another example to understand things better :
```js
function calledFunction() {
    let a = 10;
    function nestedFuncstion() {
        console.log(a); // prints 10
    }
    nestedFunction();
}
calledFunction();
```
In the above example, variable `a` is accessible inside the `nestedFunction()` function because its nested functions.
```js
function calledFunction() {
    let a = 10;
    function nestedFunction() {
        let a = 5;
        console.log(a); // prints 5
    }
    nestedFunction();
    console.log(a); // prints 10
}
calledFunction();
```
In the above example, variable `a` inside the `nestedFunction()` function has value 5 because `nestedFunction()` function finds the value of the variable `a` in the local scope first. If the variable is not declared then it looks at global scope.

Now let's understand closure,
## Closure :
It can be defined as a function along with its parent scope. Let's understand with example
```js
function calledFunction() {
    let a = 10;
    function closure() { // closure
        console.log(a); 
    }
    closure();
}
calledFunction();
``` 
In the above example, `closure()` function is closure and it has an access to its parent scope i.e. `calledFunction()` function. we can also define closure has function along with its outer function scope.

Let's understand more about closures :
```js
function calledFunction() {
    return function returnedFunction() {
        let a = 10;
        console.log(a);
    }
}
const storedFunction = calledFunction();
storedFunction();
```
In the above example, when `calledFunction()` function is called it will return `returnedFunction()` function and that will be stored in the `storedFunction` and now when `storedFunction()` function is called the returned function will execute i.e. `returnedFunction()` function.
```js
function calledFunction() {
    let a = 10;
    return function returnedFunction() {
        console.log(a);
    }
}
const storedFunction = calledFunction();
storedFunction();
```
The above code works fine because as in the definition discussed closures are bounded with its parent scope i.e. `calledFunction()` function. so whenever `returnedFunction()` is called it will print 10 which is local to `calledFunction()` function.
